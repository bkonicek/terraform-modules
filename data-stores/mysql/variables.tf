variable "db_password" {
  description = "The password for the database"
  type        = string
}

variable "db_prefix" {
  description = "Prefix for the database name"
  type        = string
}
