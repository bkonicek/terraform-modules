resource "aws_db_instance" "example" {
  identifier_prefix = "terraform-up-and-running"
  engine            = "mysql"
  allocated_storage = 10
  instance_class    = "db.t2.micro"
  name              = "${var.db_prefix}_database"
  username          = "admin"

  # How should we set the password?
  password = var.db_password
}
